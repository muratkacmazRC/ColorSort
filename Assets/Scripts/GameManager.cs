using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    //[SerializeField] GameObject SlotPanel;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this);
        }
        
    }
    // Start is called before the first frame update



    bool moving;
    private void SmoothMove(Transform t, Vector3 pos)
    {
        moving = true;
        t.DOMove(pos, .1f).OnComplete(() => { moving = false; });
    }
}
