using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] Text LevelNumberTxt;
    [SerializeField] Text CountdownTxt;
    [SerializeField] GameObject LevelCompletedPnl;
    [SerializeField] GameObject GameOverPnl;
    [SerializeField] Button TryAgainBtn;
    [SerializeField] Button NextBtn;
    [SerializeField] Text totalPointsTxt;
    public float timeRemaining;
    public int TotalPoints;

    public static UIManager Instance;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this);
        }
        TotalPoints = 0;
    }

    private void Start()
    {
        TryAgainBtn.onClick.AddListener(() => TryAgainButtonClicked());
        NextBtn.onClick.AddListener(() => NextButtonClicked());
    }

    // Update is called once per frame
    void Update()
    {
        if (timeRemaining > 0 &&  ShuffleManager.Instance.IsCompleted==false)
        {
            timeRemaining -= Time.deltaTime;
            CountdownTxt.text = "Timer: " +  ((int)timeRemaining).ToString();
        }
        else if(timeRemaining <= 0 && ShuffleManager.Instance.IsCompleted == false )
        {
            //game over 
            GameOverPnl.SetActive(true);
        }
    }

    public void PlaceTexts()
    {
        timeRemaining = LevelController.Instance.myLevelList.levels[LevelController.Instance.levelNumber].levelTime;
        LevelNumberTxt.text = "Level " + (LevelController.Instance.levelNumber + 1).ToString();
        CountdownTxt.text ="Timer: "+ timeRemaining.ToString();
    }

    public void LevelCompleted()
    {
        TotalPoints += LevelController.Instance.myLevelList.levels[LevelController.Instance.levelNumber].levelValue;
        totalPointsTxt.text = "Total Points : " + TotalPoints.ToString();
        LevelCompletedPnl.SetActive(true);
    }

    public void TryAgainButtonClicked()
    {
        GameOverPnl.SetActive(false);
        PlaceTexts();
        ShuffleManager.Instance.IsCompleted = false;
        ShuffleManager.Instance.StartShuffling();


    }

    public void NextButtonClicked()
    {
        LevelCompletedPnl.SetActive(false);
        LevelController.Instance.levelNumber++;
        PlaceTexts();
        ShuffleManager.Instance.IsCompleted = false;
        ShuffleManager.Instance.StartShuffling();
    }
}
