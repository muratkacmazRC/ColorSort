using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Item : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler
{
    public int ItemID;
    public void OnPointerDown(PointerEventData eventData)
    {
        JigsawController.Instance.ItemDown(transform);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        JigsawController.Instance.ItemEnter(transform);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        JigsawController.Instance.ItemUp();
    }
}
