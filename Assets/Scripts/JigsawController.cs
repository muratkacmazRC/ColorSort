using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class JigsawController : MonoBehaviour
{
    [SerializeField] Canvas myCanvas;
    bool isDragged;
    Transform lastItem;
    Transform enterItem;
    Vector3 lastPos;

    public static JigsawController Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && lastItem != null && isDragged)
        {
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(myCanvas.transform as RectTransform, Input.mousePosition, myCanvas.worldCamera, out pos);
            lastItem.position = myCanvas.transform.TransformPoint(pos);
        }
    }



    public void ItemDown(Transform t)
    {
        lastItem = t;
        isDragged = true;
        t.SetAsLastSibling();
        t.GetComponent<Image>().raycastTarget = false;
        lastPos = t.position;
    }


    public void ItemUp()
    {
        isDragged = false;
        lastItem.GetComponent<Image>().raycastTarget = true;
        lastItem.position = lastPos;
        ShuffleManager.Instance.ItemDropped();
    }


    public void ItemEnter(Transform t)
    {
        if (isDragged)
        {
            enterItem = t;
            Vector3 tempPos = t.position;
            t.position = lastPos;
            lastPos = tempPos;
            
            ShuffleManager.Instance.SwitchItem(t.GetComponent<Item>().ItemID, lastItem.GetComponent<Item>().ItemID);


            //if (!moving)
            //{
            //    enterItem = t;
            //    Vector3 tempPos = t.position;
            //    SmoothMove(t, lastPos);
            //    lastPos = tempPos;
            //}

            //t.position = lastPos;
        }
    }
}
