using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
using System.Linq;

public class ShuffleManager : MonoBehaviour
{
    [SerializeField] List<ItemModel> ItemsList = new List<ItemModel>();
    [SerializeField] RectTransform Items;
    [SerializeField] List<int> Indexes = new List<int>();
    public bool IsCompleted;

    Color newColor;
    public static ShuffleManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        StartShuffling();
    }

    public void StartShuffling()
    {
        ItemsList = ItemsList.OrderBy(w => w.Order).ToList();
        for (int i = 0; i < ItemsList.Count; i++)
        {
            if (ColorUtility.TryParseHtmlString(LevelController.Instance.myLevelList.levels[LevelController.Instance.levelNumber].levelColors[i], out newColor))
            {
                ItemsList[i].ItemObject.GetComponent<Image>().color = newColor;
            }
        }

        for (int i = 0; i < ItemsList.Count; i++)
        {
            ItemModel temp = ItemsList[i];

            int randomIndex = UnityEngine.Random.Range(i, ItemsList.Count);
            ItemsList[i] = ItemsList[randomIndex];
            ItemsList[randomIndex] = temp;
        }

        Indexes.Clear();

        foreach (var item in ItemsList)
        {
            Indexes.Add(item.Order);

        }


        CreateItems();
    }

    private void CreateItems()
    {
        
        if (Items.GetChildCount() > 0)
        {
            for (int i = 0; i < 5; i++)
            {
               GameObject.DestroyImmediate(Items.GetChild(0).gameObject);
            }
        }
           
        

        for (int i = 0; i < 5; i++)
        {

            RectTransform clone = Instantiate(ItemsList[i].ItemObject, Items);
            clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 400 + i*-200);
            //ItemsList.RemoveAt(0);
            
        }
    }

    public void SwitchItem(int enterid, int lastId)
    {
        if (enterid == lastId)
        {
            return;
        }
        int lastIndex = Indexes.IndexOf(lastId);
        int enterIndex = Indexes.IndexOf(enterid);
        Indexes[lastIndex] = enterid;
        Indexes[enterIndex] = lastId;
        CheckItems();
    }

    private void CheckItems()
    {
        for (int i = 0; i < Indexes.Count; i++)
        {
            if ((i + 1) != Indexes[i])
            {
                break;
            }
            if (i == 4)
            {
                IsCompleted = true;
            }
            else
            {
                IsCompleted = false;
            }
        }
    }

    public void ItemDropped()
    {
        if (IsCompleted)
        {
            Debug.Log("Completed;");
            // _PuzzlePnl.DOPlayBackwards();
            UIManager.Instance.LevelCompleted();
        }
    }
}


[Serializable]
public class ItemModel
{
    public int Order;
    public RectTransform ItemObject;
}
