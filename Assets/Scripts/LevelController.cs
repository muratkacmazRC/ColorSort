using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;

public class LevelController : MonoBehaviour
{

    public LevelList myLevelList = new LevelList();
    private string jsonString;
    public int levelNumber;

    [System.Serializable]
    public class Level
    {
        public string[] levelColors;
        public int levelTime;
        public int levelValue;
    }
    [System.Serializable]
    public class LevelList
    {
        public Level[] levels;
    }


    public static LevelController Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

        }
        else
        {
            Destroy(this);
        }
        levelNumber = 0;
    }

    async void Start()
    {
        
        jsonString = File.ReadAllText(Application.dataPath + "/Scripts/colorSort.json");
        myLevelList = JsonUtility.FromJson<LevelList>(jsonString);
        
        await Task.Run(() => UIManager.Instance.PlaceTexts());
        //object p = await UIManager.Instance.PlaceTexts();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
